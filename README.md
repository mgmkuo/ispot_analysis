## iSpot Reporting
Repo for 
- Identifying ROS and Vantage units in iSpot reports
- Creating excel report of conversion analysis  

### Files used to execute code:
1. run_identify.py : Use for identifying ROS and Vantage units in iSpot reports. 
This is requested regularly (usually weekly). VARP ticket is usually titled "ROS vs. Vantage identify"
2. run_analysis.py : Use for generating excel report of iSpot. 
This is requested less often (quarterly).
3. config.py : file to enter file name, client name and credentials.  
Use this config file for both run_identify.py and run_analysis.py.

### How to use
#### For identifying ROS and Vantage units in iSpot reports
1. Rename "Optimized vs ROS" column in xlsx to "ROS vs Optimized"  
(Sorry, column name is hard coded in)
2. Enter file path and client name in config.py
3. Execute run_identify.py (python run_identify.py)

#### For creating excel report of conversion analysis
1. Enter file path, client name and start date of concurrent ROS (can find on onboarding) in config.py
2. Execute run_analysis.py


#### Dependencies:
Listed in requirements.txt
  





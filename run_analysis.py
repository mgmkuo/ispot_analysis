from ispot_analysis.ispot_analysis import iSpot
import config

engine_str = "redshift+psycopg2://" + config.user_name + ":" + config.password + "@viacom-vantage3.caijrp92niv7.us-east-1.redshift.amazonaws.com:5439/dev"

filename = config.filename
client_name = config.client_name
ros_concurrent_start_date = config.ros_concurrent_start_date

create_ispot = iSpot(client_name, filename, engine_str, ros_concurrent_start_date)
create_ispot.create_xlsx()
print('\nAll Done!\n')
print('Original iSpot df size: {}'.format(len(create_ispot.ispot_data_orig)))
print('Final matched iSpot df: {}'.format(len(create_ispot.ispot_arl_matched_final)))
print('Remaining unmatched: {}\n'.format(len(create_ispot.unmatched_df)))
print(create_ispot.unmatched_df[['occurrence_id', 'show', 'network', 'air_date_time_et']])
print('\n')




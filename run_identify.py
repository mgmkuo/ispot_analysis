from ispot_analysis.ispot_identify import iSpotIdentify
from ispot_analysis.ispot_analysis import DuplicateMatches
import config


engine_str = "redshift+psycopg2://" + config.user_name + ":" + config.password + "@viacom-vantage3.caijrp92niv7.us-east-1.redshift.amazonaws.com:5439/dev"

filename = config.filename
client_name = config.client_name
ros_concurrent_start_date = None

print('Processing: {} {}'.format(client_name, filename))

create_ispot = iSpotIdentify(client_name, filename, engine_str)
create_ispot.print_matched_data()

print('\nAll Done!\n')
print('Original iSpot df size: {}'.format(len(create_ispot.ispot_data_orig)))
print('Final matched iSpot df: {}'.format(len(create_ispot.ispot_arl_matched_final)))

if len(create_ispot.ispot_arl_matched_final) > len(create_ispot.ispot_data_orig):
    raise DuplicateMatches('Duplicate ispot matches found. Please revise code')

if len(create_ispot.unmatched_df) > 0:
    print('\nRemaining unmatched iSpot (iSpot shows not in as run log): {}\n'.format(len(create_ispot.unmatched_df)))
    print("(network, show, air_date_time_et, occurrence_id)")
    for i in create_ispot.unmatched_df.index:
        time, net, show, show_id = create_ispot.unmatched_df.loc[i, ['air_date_time_et', 'network', 'show', 'occurrence_id']]
        print("{}, {}, {}, {}".format(net, show, time, show_id))

elif len(create_ispot.unmatched_df) == 0:
    print('\nAll iSpot shows matched')

if len(create_ispot.unmatched_asrun) > 0:
    print('\nAs run shows not in iSpot:\n')
    print(create_ispot.unmatched_asrun[['cal_spot_dt_tm', 'network', 'program_posted_title']])
    unmatched = create_ispot.unmatched_asrun[['cal_spot_dt_tm', 'network', 'program_posted_title']]
    unmatched.to_csv('{}_as_run_unmatched.csv'.format(filename), index=False)

elif len(create_ispot.unmatched_asrun) == 0:
    print("All shows in as run log matched in iSpot")

# checking for simulcasts
unmatched = create_ispot.unmatched_df
if unmatched.empty:
    pass
else:
    unmatched['air_date_hr'] = unmatched['air_date_time_et'].apply(lambda x: x.hour)
    unmatched['air_date_min'] = unmatched['air_date_time_et'].apply(lambda x: x.minute)
    print('\nPossible simulcasts: ')
    print(unmatched[unmatched.duplicated(subset=['air_date_hr','air_date_min','air_date_et'],
                         keep=False)][['air_date_time_et', 'network', 'show', 'occurrence_id']])
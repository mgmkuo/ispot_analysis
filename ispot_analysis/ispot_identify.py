"""
For identifying ROS and Vantage spots in iSpot data

"""
import pandas as pd
from ispot_analysis.ispot_analysis import iSpot
from datetime import timedelta

class BlankGuidTelecastID(Exception):
    pass

class iSpotIdentify(iSpot):
    def __init__(self, client_name, filename, engine_str):
        iSpot.__init__(self, client_name, filename, engine_str, ros_concurrent_start_date=None)
        self.unmatched_asrun = self.find_unmatched_asrun()

    def print_matched_data(self):
        final_df = pd.ExcelFile(self.filename).parse(0)
        print('Orig length: ' + str(len(final_df)))
        final_df = final_df.merge(self.ispot_arl_matched_final[['occurrence_id', 'rate_class', 'unit_traffic_status']],
                                  left_on='Occurrence ID', right_on='occurrence_id')
        final_df = final_df.drop(['ROS vs Optimized', 'occurrence_id'], axis=1)
        final_df = final_df.rename(index=str, columns={'rate_class': 'ROS vs Optimized'})
        final_df['ROS vs Optimized'] = [iSpot.map_rate_class(x) for x in final_df['ROS vs Optimized']]
        for row in range(len(final_df)):
            if final_df['unit_traffic_status'][row] != 'Aired':
                final_df['ROS vs Optimized'][row] == 'Not Aired'
        final_df = final_df.drop('unit_traffic_status', axis=1)
        orig_df = pd.ExcelFile(self.filename).parse(0)
        final_df = final_df.append(orig_df[~orig_df['Occurrence ID'].isin(final_df['Occurrence ID'])])
        print('Final length: ' + str(len(final_df)))
        final_df = final_df.drop_duplicates(subset='Occurrence ID')

        f_name = self.filename.split('.xlsx')[0]
        final_df.to_csv(f_name + '_conversion.csv')
        print("File saved as {}".format(f_name + '_conversion.csv'))

    def find_unmatched_asrun(self):
        arl_sub = self.asrun_data[(self.asrun_data.cal_spot_dt_tm >= self.ispot_data_orig['air_date_time_et'].min()) & (
                self.asrun_data.cal_spot_dt_tm <= self.ispot_data_orig['air_date_time_et'].max())]

        arl_cut_off_date_df = arl_sub[arl_sub.guid_telecast_id.isna()]
        # newer entries in as run log might not have guid_telecast_id so need to filter those out
        if len(arl_cut_off_date_df) != 0:
            arl_cut_off_date = str(arl_cut_off_date_df.cal_spot_dt_tm.min().date())
            final_sub = self.ispot_arl_matched_final[self.ispot_arl_matched_final['air_date_time_et'] < arl_cut_off_date]
            arl_sub = arl_sub[arl_sub['cal_spot_dt_tm'] <= arl_cut_off_date]
        elif len(arl_cut_off_date_df) == 0:
            final_sub = self.ispot_arl_matched_final

        # finding arl that aren't in the final merged df using guid telecast id
        arl_missing = arl_sub[~arl_sub.guid_telecast_id.isin(final_sub.guid_telecast_id.unique())]
        arl_missing = arl_missing[arl_missing['network'] != 'BET']

        return arl_missing
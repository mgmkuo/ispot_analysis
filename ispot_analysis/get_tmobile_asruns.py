import pandas as pd


def get_as_runs(engine,client_name):
    """
    as run call for conversion report

    :param engine:
    :param client_name:
    :return:
    """
    daypart_dict = {1: '6:00AM - 8:59AM', 2: '9:00AM - 3:59PM', 3: '4:00PM - 6:59PM', 4: '7:00PM - 10:59PM',
                    5: '11:00PM - 2:59AM', 6: '3:00AM - 5:59AM'}

    query = """
            SELECT client_name,
                   in_vn,
                   br_dt_key,
                   network,
                   br_spot_dt_tm,
                   cal_spot_dt_tm,
                   date(cal_spot_dt_tm) as cal_date,
                   guid_telecast_id,
                   pfid,
                   brand as campaign_brand,
                   reporting_category,
                   program_posted_title,
                   unit_type,
                   eq_units,
                   gross_revenue,
                   gross_earned_revenue,
                   net_earned_revenue
            FROM viacom.as_run_log
            WHERE client_name = '{client}'
            AND   DATE (cal_spot_dt_tm) >= '2017-01-01'
            --AND   unit_traffic_status = 'Aired'
            ;
            """.format(client=client_name)
    asrun = pd.read_sql(query, engine)

    # format as run
    asrun['network'] = asrun['network'].map(lambda x: x.strip())
    asrun['network'] = asrun['network'].map(lambda x: 'TVL' if x == 'TVLC' else x)
    asrun['network'] = asrun['network'].map(lambda x: 'MTV' if x == 'AMTV' else x)
    asrun['network'] = asrun['network']
    asrun['cal_date'] = pd.to_datetime(asrun['cal_date'])
    asrun['cal_spot_dt_tm'] = pd.to_datetime(asrun['cal_spot_dt_tm'])
    asrun['cal_week'] = asrun['cal_spot_dt_tm'].dt.week
    asrun['cal_year'] = asrun['cal_spot_dt_tm'].dt.year
    asrun['hour'] = asrun['cal_spot_dt_tm'].map(lambda x: x.hour)
    asrun['daypart_time_range'] = asrun['hour'].map(
        lambda x: 1 if 6 <= x < 9 else 2 if 9 <= x < 16 else 3 if 16 <= x < 19
        else 4 if 19 <= x < 23 else 6 if 3 <= x < 6 else 5)
    asrun['vn_daypart'] = asrun['daypart_time_range'].map(lambda x: daypart_dict[x])
    asrun.rename(columns={'eq_units': 'Equivalized Units'}, inplace=True)
    asrun = asrun[(asrun['unit_type'] != 'No Charge/No Post') & (asrun['Equivalized Units'] != 0.16)]
    return asrun



def get_as_runs_for_iSpot_identify(conn, client):
    """
    As run call for identifying ROS and Vantage in iSpot

    :param conn:
    :param client:
    :return:
    """
    daypart_dict = {1: '6:00AM - 8:59AM', 2: '9:00AM - 3:59PM', 3: '4:00PM - 6:59PM', 4: '7:00PM - 10:59PM',
                    5: '11:00PM - 2:59AM', 6: '3:00AM - 5:59AM'}

    query = """
            SELECT client_name,
                   in_vn,
                   br_dt_key,
                   network,
                   br_spot_dt_tm,
                   cal_spot_dt_tm,
                   date(cal_spot_dt_tm) as cal_date,
                   guid_telecast_id,
                   pfid,
                   rate_class,
                   brand as campaign_brand,
                   reporting_category,
                   program_posted_title,
                   unit_traffic_status,
                   unit_type,
                   eq_units,
                   gross_revenue,
                   gross_earned_revenue,
                   net_earned_revenue
            FROM viacom.as_run_log
            WHERE client_name = '{client}'
            AND   DATE (cal_spot_dt_tm) >= '2017-01-01'
            --AND   unit_traffic_status = 'Aired'
            ;
            """.format(client=client)
    asrun = pd.read_sql(query, conn)

    # format as run
    asrun['network'] = asrun['network'].map(lambda x: x.strip())
    asrun['network'] = asrun['network'].map(lambda x: 'TVL' if x == 'TVLC' else x)
    asrun['network'] = asrun['network'].map(lambda x: 'MTV' if x == 'AMTV' else x)
    asrun['cal_date'] = pd.to_datetime(asrun['cal_date'])
    asrun['cal_spot_dt_tm'] = pd.to_datetime(asrun['cal_spot_dt_tm'])
    asrun['cal_week'] = asrun['cal_spot_dt_tm'].dt.week
    asrun['cal_year'] = asrun['cal_spot_dt_tm'].dt.year
    asrun['hour'] = asrun['cal_spot_dt_tm'].map(lambda x: x.hour)
    asrun['daypart_time_range'] = asrun['hour'].map(
        lambda x: 1 if 6 <= x < 9 else 2 if 9 <= x < 16 else 3 if 16 <= x < 19
        else 4 if 19 <= x < 23 else 6 if 3 <= x < 6 else 5)
    asrun['vn_daypart'] = asrun['daypart_time_range'].map(lambda x: daypart_dict[x])
    asrun.rename(columns={'eq_units': 'Equivalized Units'}, inplace=True)
    asrun = asrun[(asrun['unit_type'] != 'No Charge/No Post') & (asrun['Equivalized Units'] != 0.16)]

    return asrun
ORDERED_DAYPARTS = ["6:00AM - 8:59AM", "9:00AM - 3:59PM", "4:00PM - 6:59PM", "7:00PM - 10:59PM",
                    "11:00PM - 2:59AM", "3:00AM - 5:59AM"]


def create_weekly_daypart_tab(writer, formats, merged_data):
    row_number = 1
    # weekly breakdowns
    # get ordered list of weeks
    weeks = sorted(merged_data.start_date.unique())
    weeks = list(['Total']) + sorted(merged_data.start_date.unique())

    week_col = chr(len(weeks) + 97 + 1).upper() if len(weeks) < 25 else \
        (chr(int((len(weeks) + 1 + 1) / 26 + 97 - 1)) + chr(int((len(weeks) + 1 + 1) % 26 + 97))).upper()
    # overall results
    weekly_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units',
                               'campaign', 'start_date']]
    weekly_campaign_data = weekly_data.groupby(['campaign', 'start_date']).sum().reset_index()
    weekly_overall_results = weekly_data.groupby(['start_date']).sum().reset_index()
    weekly_overall_results['campaign'] = 'Total'
    weekly_overall_results = weekly_campaign_data.append(weekly_overall_results, ignore_index=True)

    quarter_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'campaign', 'quarter']]
    quarter_data = quarter_data.groupby(['campaign','quarter']).sum().reset_index()
    quarter_data = quarter_data.rename(index=str, columns={'quarter':'start_date'})

    quarter_total_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'campaign']]
    quarter_total_data['start_date'] = 'Total'
    quarter_total_data = quarter_total_data.groupby(['start_date', 'campaign']).sum().reset_index()

    quarter_total_row_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'quarter', 'campaign']]
    quarter_total_row_data['campaign'] = 'Total'
    quarter_total_row_data = quarter_total_row_data.groupby(['campaign','quarter']).sum().reset_index()
    quarter_total_row_data = quarter_total_row_data.rename(index=str, columns={'quarter':'start_date'})

    quarter_total_total_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'start_date', 'campaign']]
    quarter_total_total_data['start_date'] = 'Total'
    quarter_total_total_data['campaign'] = 'Total'
    quarter_total_total_data = quarter_total_total_data.groupby(['campaign','start_date']).sum().reset_index()

    quarter_data = quarter_data.append(quarter_total_total_data)
    quarter_data = quarter_data.append(quarter_total_row_data)
    quarter_data = quarter_data.append(quarter_total_data)


    weekly_overall_results = weekly_overall_results.append(quarter_data)
    weekly_overall_results['Conversion Rate'] = weekly_overall_results['matched_conversion_events'] / weekly_overall_results['matched_tv_population_impressions']
    weekly_overall_results["campaign"] = weekly_overall_results["campaign"].astype('category',
                                                                                   categories=["ROS", "Vantage",
                                                                                               "Total"])
    weekly_overall_pivot = weekly_overall_results.pivot_table(index=["campaign", 'start_date'],
                                                              values=["Conversion Rate", 'Equivalized Units'])
    weekly_overall_pivot = weekly_overall_pivot.stack().unstack(-2).reset_index()
    weekly_overall_pivot.rename(columns={'level_1': ' '}, inplace=True)
    weekly_overall_pivot = weekly_overall_pivot.reindex(columns=['campaign', ' '] + weeks)

    # write to excel
    weekly_overall_pivot.to_excel(writer, sheet_name='Weekly Daypart', startrow=row_number, index=False)
    weekly_daypart_tab = writer.sheets['Weekly Daypart']
    weekly_daypart_tab.hide_gridlines(2)
    weekly_daypart_tab.set_column('A:A', 10, formats.center_format)
    weekly_daypart_tab.set_column('B:B', 20, formats.center_format)
    weekly_daypart_tab.set_column('C:' + week_col, 10, formats.center_format)

    # format overall
    weekly_daypart_tab.merge_range('A' + str(row_number + 2) + ':A' + str(row_number + 3),
                                   'ROS', formats.rowheader_format)
    weekly_daypart_tab.merge_range('A' + str(row_number + 4) + ':A' + str(row_number + 5),
                                   'Vantage', formats.rowheader_format)
    weekly_daypart_tab.merge_range('A' + str(row_number + 6) + ':A' + str(row_number + 7),
                                   'Total', formats.rowheader_format)
    weekly_daypart_tab.merge_range('A' + str(row_number + 1) + ':B' + str(row_number + 1), 'Overall',
                                   formats.header_format)
    weekly_daypart_tab.conditional_format('C' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                          {'type': 'no_errors', 'format': formats.header_format})
    weekly_daypart_tab.conditional_format('C' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                          {'type': 'no_errors', 'format': formats.date_format})
    weekly_daypart_tab.set_row(row_number, 25)
    # move down row num
    row_number += 1
    weekly_daypart_tab.conditional_format('B' + str(row_number + 1) + ':B' + str(row_number + len(weekly_overall_pivot)),
                                          {'type': 'no_errors', 'format': formats.white_bold_format})
    row_number += 1
    for r in range(0, len(weekly_overall_pivot), 2):
        weekly_daypart_tab.conditional_format('C' + str(row_number + r) + ':' + week_col + str(row_number + r),
                                              {'type': 'no_errors', 'format': formats.percentage_format})
        weekly_daypart_tab.conditional_format('C' + str(row_number + r) + ':' + week_col + str(row_number + r),
                                              {'type': 'no_errors', 'format': formats.blue_bold_format})
        weekly_daypart_tab.conditional_format('C' + str(row_number + r + 1) + ':' + week_col + str(row_number + r + 1),
                                              {'type': 'no_errors', 'format': formats.float_format})
        weekly_daypart_tab.conditional_format('C' + str(row_number + r + 1) + ':' + week_col + str(row_number + r + 1),
                                              {'type': 'no_errors', 'format': formats.green_bold_format})

    # daypart weekly results
    # for each daypart
    weekly_daypart_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events',
                                       'Equivalized Units', 'campaign', 'vn_daypart', 'start_date']]

    row_number += len(weekly_overall_pivot)
    for dp in ORDERED_DAYPARTS:
        weekly_dp_spc_data = weekly_daypart_data[weekly_daypart_data['vn_daypart'] == dp]
        weekly_dp_spc_results = weekly_dp_spc_data.groupby(['campaign', 'start_date']).sum().reset_index()
        weekly_dp_spc_total_results = weekly_dp_spc_results.groupby(['start_date']).sum().reset_index()
        weekly_dp_spc_total_results['campaign'] = 'Total'
        weekly_dp_spc_results = weekly_dp_spc_results.append(weekly_dp_spc_total_results, ignore_index=True)

        dp_quarter_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'vn_daypart', 'campaign', 'quarter']]
        dp_quarter_data = dp_quarter_data[dp_quarter_data['vn_daypart'] == dp]
        dp_quarter_data = dp_quarter_data.groupby(['campaign','quarter']).sum().reset_index()
        dp_quarter_data = dp_quarter_data.rename(index=str, columns={'quarter':'start_date'})

        dp_total_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units','campaign','vn_daypart']]
        dp_total_data = dp_total_data[dp_total_data.vn_daypart == dp]
        dp_total_data['start_date'] = 'Total'
        dp_total_data = dp_total_data.groupby(['start_date','campaign']).sum().reset_index()
        dp_quarter_data = dp_quarter_data.append(dp_total_data)

        dp_total_row_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'quarter', 'vn_daypart']]
        dp_total_row_data = dp_total_row_data[dp_total_row_data.vn_daypart == dp]
        dp_total_row_data['campaign'] = 'Total'
        dp_total_row_data = dp_total_row_data.groupby(['campaign','quarter']).sum().reset_index()
        dp_total_row_data = dp_total_row_data.rename(index=str, columns={'quarter':'start_date'})

        dp_total_total_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'start_date', 'campaign', 'vn_daypart']]
        dp_total_total_data = dp_total_total_data[dp_total_total_data.vn_daypart == dp]
        dp_total_total_data['start_date'] = 'Total'
        dp_total_total_data['campaign'] = 'Total'
        dp_total_total_data = dp_total_total_data.groupby(['campaign','start_date']).sum().reset_index()

        weekly_dp_spc_results = weekly_dp_spc_results.append(dp_total_total_data, ignore_index=True)
        weekly_dp_spc_results = weekly_dp_spc_results.append(dp_total_row_data, ignore_index=True)
        weekly_dp_spc_results = weekly_dp_spc_results.append(dp_quarter_data, ignore_index=True)


        weekly_dp_spc_results['Conversion Rate'] = weekly_dp_spc_results['matched_conversion_events'] / weekly_dp_spc_results['matched_tv_population_impressions']
        weekly_dp_spc_results["campaign"] = weekly_dp_spc_results["campaign"].astype('category', categories=["ROS", "Vantage", "Total"])
        weekly_dp_spc_pivot = weekly_dp_spc_results.pivot_table(index=["campaign", 'start_date'], values=["Conversion Rate", 'Equivalized Units'])
        weekly_dp_spc_pivot = weekly_dp_spc_pivot.stack().unstack(-2).reset_index()
        weekly_dp_spc_pivot.rename(columns={'level_1': dp}, inplace=True)
        weekly_dp_spc_pivot = weekly_dp_spc_pivot.reindex(columns=['campaign', dp] + weeks)
        weekly_dp_spc_pivot.to_excel(writer, sheet_name='Weekly Daypart', startrow=row_number, index=False)

        # add formatting
        # header
        weekly_daypart_tab.merge_range('A' + str(row_number + 1) + ':B' + str(row_number + 1), dp,
                                       formats.header_format)
        weekly_daypart_tab.conditional_format('B' + str(row_number + 1) + ':B' + str(row_number + len(weekly_dp_spc_pivot) + 1),
                                              {'type': 'no_errors', 'format': formats.white_bold_format})
        weekly_daypart_tab.conditional_format('C' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                              {'type': 'no_errors', 'format': formats.header_format})
        weekly_daypart_tab.conditional_format('C' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                              {'type': 'no_errors', 'format': formats.date_format})
        weekly_daypart_tab.set_row(row_number, 25)

        start_row = 0
        row_number += 1
        while start_row + 2 <= len(weekly_dp_spc_pivot):
            if weekly_dp_spc_pivot.loc[start_row]['campaign'] == weekly_dp_spc_pivot.loc[start_row + 1]['campaign']:
                campaign = weekly_dp_spc_pivot.loc[start_row]['campaign']
                weekly_daypart_tab.merge_range('A' + str(row_number + 1) + ':A' + str(row_number + 2), campaign,
                                               formats.rowheader_format)
                weekly_daypart_tab.conditional_format('C' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                                      {'type': 'no_errors', 'format': formats.percentage_format})
                weekly_daypart_tab.conditional_format('C' + str(row_number + 2) + ':' + week_col + str(row_number + 2),
                                                      {'type': 'no_errors', 'format': formats.float_format})
                if campaign == 'Total':
                    weekly_daypart_tab.conditional_format('C' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                                          {'type': 'no_errors', 'format': formats.blue_bold_format})
                    weekly_daypart_tab.conditional_format('C' + str(row_number + 2) + ':' + week_col + str(row_number + 2),
                                                          {'type': 'no_errors', 'format': formats.green_bold_format})
                else:
                    weekly_daypart_tab.conditional_format('C' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                                          {'type': 'no_errors', 'format': formats.blue_format})
                    weekly_daypart_tab.conditional_format('C' + str(row_number + 2) + ':' + week_col + str(row_number + 2),
                                                          {'type': 'no_errors', 'format': formats.green_format})
                start_row += 2
                row_number += 2

        row_number += 1
    return writer

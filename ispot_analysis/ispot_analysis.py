"""
For creating excel print out of iSpot data

"""

import datetime
import pandas as pd
from sqlalchemy import create_engine
from ispot_analysis.excel_formatter import Formats
import ispot_analysis.get_tmobile_asruns as as_run_call
from ispot_analysis.overall_excel_tab_dev import create_overall_tab
from ispot_analysis.program_tab import create_program_tab
from ispot_analysis.weekly_network_tab import create_weekly_network_tab
from ispot_analysis.weekly_daypart_tab import create_weekly_daypart_tab
from ispot_analysis.weekly_network_daypart_tab import create_weekly_net_dp_tab

import pdb

class NewExcelLayout(Exception):
    pass

class DuplicateMatches(Exception):
    pass

class dfFormatting:
    def get_broadcast_sow_from_week(self, br_dt_key):
        # calculate the first day of calendar year
        first_day_of_cal_yr = datetime.date(year=int(br_dt_key / 100), month=1, day=1)
        first_day_of_br_yr = first_day_of_cal_yr - datetime.timedelta(days=first_day_of_cal_yr.weekday())
        # return the start date
        return first_day_of_br_yr + datetime.timedelta(days=7 * int((br_dt_key % 100 - 1)))

    def get_quarter_number(self, month):
        if month < 4:
            return 1
        elif month < 7:
            return 2
        elif month < 10:
            return 3
        else:
            return 4

    def annoying_date(self, x):
        if x == datetime.date(year=2018, month=1, day=1):
            return datetime.date(year=2018, month=12, day=31)
        else:
            return x

    def parse_program_title(self, show):
        """
        For matching titles like "Ice Age: Dawn of the Dinosaurs [Movie]"
        to "Ice Age: Dawn of the Dinosaurs (Ice Age: Dawn of)"
        """
        for x in '([':
            show = show.split(x)[0].strip()
            show = show.lower()
        return show

    def parse_program_title_by_trimming(self, show):
        """
        For matching titles like "Ice Age: The Meltdown [Movie]"
        to "Ice Age 2: The Meltdown (2006) (Ice Age 2)"
        """
        return show.split(':')[0].lower().strip()

    @staticmethod
    def map_rate_class(x):
        if x == 'VANTAGE':
            return 'Vantage'
        else:
            return 'ROS'


class iSpot(dfFormatting):
    def __init__(self, client_name, filename, engine_str, ros_concurrent_start_date):
        self.client_name = client_name
        self.filename = filename
        self.engine = create_engine(engine_str)
        self.ros_concurrent_start_date = ros_concurrent_start_date
        if ros_concurrent_start_date:
            self.asrun_data = as_run_call.get_as_runs(self.engine, self.client_name)
        else:
            self.asrun_data = as_run_call.get_as_runs_for_iSpot_identify(self.engine, self.client_name)
        self.ispot_data_orig = self.import_ispot_data(filename)
        self.ispot_arl_matched_v1 = self.merge_ispot_and_asrun(self.ispot_data_orig, 180)
        self.ispot_arl_matched_v2 = self.rematch_missing_data()
        self.ispot_arl_matched_final = self.format_merged_table()  # df for excel print out


    def import_ispot_data(self, filename):
        net_signs = {'Comedy Central': 'CMDY', 'Nick': 'NICK', 'Nick@Nite': 'NAN', 'Paramount Network': 'SPIKE',
                     'MTV': 'MTV', 'MTV2': 'MTV2', 'VH1': 'VH1', 'Teen Nick': 'TNNK', 'Nick Jr.': 'NKJR',
                     'Nick Toons': 'NKTNS', 'TV LAND': 'TVL', 'CMTV': 'CMT', 'Logo': 'LOGO'}

        try:
            ispot = pd.ExcelFile(filename).parse(1)
        except IndexError:
            ispot = pd.ExcelFile(filename).parse(0)
        except:
            raise NewExcelLayout("This excel spreadsheet format is new. Adjust the code.")

        # (enter option to do two quarters) ??
        # ispot = ispot.append(ispot1, ignore_index=True)

        ispot.columns = [x.lower().replace(" ", "_") for x in ispot.columns]
        ispot = ispot[ispot.network.isin(['Comedy Central', 'Nick', 'Nick@Nite', 'Paramount Network',
                     'MTV', 'MTV2', 'VH1', 'Teen Nick', 'Nick Jr.',
                     'Nick Toons', 'TV LAND', 'CMTV', 'Logo'])]
        ispot['net_sign'] = ispot['network'].map(lambda x: net_signs[x])
        ispot['net_name'] = ispot['network'].map(lambda x: x.strip())
        ispot['network'] = ispot['net_sign'].map(lambda x: x.strip())
        ispot['air_date_et'] = pd.to_datetime(ispot['air_date_et'])
        ispot['air_date_time_et'] = pd.to_datetime(ispot['air_date_time_et'])

        # ispot['matched_conversion_events'].fillna(0, inplace=True)
        return ispot

    def merge_ispot_and_asrun(self, df_ispot, time_diff_sec):
        """
        Inner joining based on cal spot time and network
        :return:
        merged asrun and ispot table
        """
        print('\nInput iSpot dataframe size: {}'.format(len(df_ispot)))
        asrun_merge = df_ispot.merge(self.asrun_data,
                                                 left_on=['network', 'air_date_et'],
                                                 right_on=['network', 'cal_date'])
        asrun_merge['time_diff'] = abs(asrun_merge['cal_spot_dt_tm'] - asrun_merge['air_date_time_et'])
        asrun_merge = asrun_merge[asrun_merge['time_diff'] < datetime.timedelta(seconds=time_diff_sec)]

        print('\nNumber of as run matches to iSpot dataframe: {}'.format(len(asrun_merge)))
        return asrun_merge

    def rematch_missing_data(self):
        """
        Can add on other ways of parsing titles here.
        Use parse_option as flag
        """
        final_df = self.ispot_arl_matched_v1
        print("\n---------------- MATCHING based on time +/- 5 minutes --------------------------------\n")
        final_df = self._rematch_missing_data_by_time(final_df, 300)
        print("\n---------------- MATCHING based on time +/- 10 minutes --------------------------------\n")
        final_df = self._rematch_missing_data_by_time(final_df, 600)
        print("\n---------------- MATCHING based on time +/- 15 minutes -------------------------------\n")
        final_df = self._rematch_missing_data_by_time(final_df, 900)
        print("\n---------------- MATCHING based on name (and time +/- 15 minutes) ------------------------------\n")
        final_df = self._rematch_missing_data_by_name(final_df, 900, parse_option=1)
        print("\n ---------------- MATCHING based on shortened show name (and time +/- 15 minutes) ---------------\n")
        final_df = self._rematch_missing_data_by_name(final_df, 900, parse_option=2)
        self.unmatched_df = self.identify_unmatched_ids(final_df)  # iSpot shows not in as run log
        return final_df

    def _rematch_missing_data_by_time(self, final_df, time_diff_sec):
        """
        Matching based on time for cal spot times within time_diff_sec of each other
        Appends new matches to already matched df
        """
        unmatched_df = self.identify_unmatched_ids(final_df)
        ispot_arl_merged = self.merge_ispot_and_asrun(unmatched_df, time_diff_sec)
        print('\nMatched iSpot shows this round:')
        print(ispot_arl_merged[['occurrence_id',
                                'guid_telecast_id',
                                'show',
                                'network',
                                'air_date_time_et',
                                'cal_spot_dt_tm',
                                'rate_class']])

        if len(ispot_arl_merged.occurrence_id.unique()) != len(ispot_arl_merged):
            print('Duplicate matches found!')
            duplicated = ispot_arl_merged[ispot_arl_merged.duplicated(subset='occurrence_id', keep=False)]
            print(duplicated[['occurrence_id',
                                'guid_telecast_id',
                                'show',
                                'network',
                                'air_date_time_et',
                                'cal_spot_dt_tm',
                                'rate_class']])
            if len(duplicated.rate_class.unique()) != 1:
                raise DuplicateMatches('Occurrence ID {} matched to as runs with different rate classes. Please resolve.'.format(
                    duplicated.occurrence_id.unique()
                ))
            else:
                ispot_arl_merged = ispot_arl_merged.sort_values(by=['occurrence_id', 'time_diff'])
                ispot_arl_merged = ispot_arl_merged.drop_duplicates(subset='occurrence_id', keep='first')
                print('\nFinal matched iSpot shows this round:')
                print(ispot_arl_merged[['occurrence_id',
                                        'guid_telecast_id',
                                        'show',
                                        'network',
                                        'air_date_time_et',
                                        'cal_spot_dt_tm',
                                        'rate_class']])

        final_df = final_df.append(ispot_arl_merged)
        # pdb.set_trace()
        print(final_df.shape)
        return final_df

    def _rematch_missing_data_by_name(self, final_df, time_diff_sec, parse_option=None):
        """
        Catches shows with mislabeled networks.
        In general, these shows are labeled as NICK in the iSpot data when they actually ran on other NICK nets.
        Right now matching to iSpot data
        """
        unmatched_df = self.identify_unmatched_ids(final_df)
        for occur_id in unmatched_df.occurrence_id:
            ispot_vals = self.ispot_data_orig[self.ispot_data_orig['occurrence_id'] == occur_id][['air_date_time_et',
                                                                                                  'show',
                                                                                                  'network']].values[0]
            air_time, show, network_ispot = ispot_vals[0], ispot_vals[1], ispot_vals[2]
            if parse_option == 1:
                show = self.parse_program_title(show)
            elif parse_option == 2:
                show = self.parse_program_title_by_trimming(show)
            index_arl_matching_time = self.asrun_data[
                (self.asrun_data['cal_spot_dt_tm'] > (air_time - datetime.timedelta(seconds=time_diff_sec)))
                & (self.asrun_data['cal_spot_dt_tm'] < (air_time + datetime.timedelta(seconds=time_diff_sec)))].index

            matched = []
            if len(index_arl_matching_time) != 0:
                for j in index_arl_matching_time:
                    if show in self.asrun_data.loc[j, 'program_posted_title'].lower():
                        print('\nMatched iSpot {} {} {} to asrun log {} {} {}'.format(
                                                          network_ispot,
                                                          show,
                                                          air_time,
                                                          self.asrun_data.loc[j, 'network'],
                                                          self.asrun_data.loc[j, 'program_posted_title'],
                                                          self.asrun_data.loc[j, 'cal_spot_dt_tm']))
                        matched.append(j)

            if matched:
                print('\n>>> As run index of all matched shows: {}'.format(matched))

            if len(matched) == 1:
                # (Matching to guid telecast id and cal spot dt tm instead of index to keep orientation the same
                # for later concatenating)
                index_arl_matched = matched.pop()
                guid_telecast_id = self.asrun_data.loc[index_arl_matched, 'guid_telecast_id']
                cal_spot_dt_tm = self.asrun_data.loc[index_arl_matched, 'cal_spot_dt_tm']
                arl_match = self.asrun_data[(self.asrun_data['guid_telecast_id'] == guid_telecast_id) &
                                            (self.asrun_data['cal_spot_dt_tm'] == cal_spot_dt_tm)]
                ispot_match = self.ispot_data_orig[self.ispot_data_orig['occurrence_id'] == occur_id]

                if arl_match.empty:
                    print('As run show has no guid telecast ID. Matching by other columns...')
                    print('guid_telecast_id: {}, cal_spot_dt_tm: {}'.format(guid_telecast_id, cal_spot_dt_tm))
                    arl_match = pd.DataFrame(self.asrun_data.loc[index_arl_matched, ]).transpose()
                ispot_arl_merged = pd.concat([ispot_match, arl_match], ignore_index=True, sort=False)

                # coalescing ispot with asrun: ispot precedes as run right now (2/27/19)
                ispot_arl_merged = ispot_arl_merged.loc[0, ].combine_first(ispot_arl_merged.loc[1, ])
                final_df = final_df.append(ispot_arl_merged)
                print('>>> Going with network {}. Final df size: {}'.format(ispot_arl_merged.network, final_df.shape))
                # pdb.set_trace()
        return final_df

    def identify_unmatched_ids(self, df):
        missing_spots_df = self.ispot_data_orig[~(self.ispot_data_orig.occurrence_id.isin(df.occurrence_id))]
        print('\nUnmatched shows:\n')
        print(missing_spots_df[['network', 'show', 'air_date_time_et', 'occurrence_id']])
        return missing_spots_df

    def format_merged_table(self):
        asrun_merge = self.ispot_arl_matched_v2.copy()
        asrun_merge['campaign'] = asrun_merge['in_vn'].map(lambda x: 'Vantage' if x else 'ROS')
        asrun_merge['br_start_date'] = asrun_merge['br_dt_key'].map(lambda x: self.get_broadcast_sow_from_week(x))
        asrun_merge['start_date'] = asrun_merge.apply(
            lambda x: self.get_broadcast_sow_from_week(x['cal_year'] * 100 + x['cal_week']), axis=1)
        asrun_merge['start_date'] = pd.to_datetime(asrun_merge['start_date']).dt.date

        asrun_merge['start_date'] = asrun_merge['start_date'].map(lambda x: self.annoying_date(x))
        asrun_merge['quarter'] = asrun_merge['start_date'].map(
            lambda x: str(x.year) + " Q" + str(self.get_quarter_number(x.month)))
        # asrun_merge['quarter'] = asrun_merge['quarter'].map(lambda x: annoying_quarter(x))

        for row in range(len(asrun_merge)):
            if asrun_merge['quarter'].iloc[row] == '2018 Q4' and asrun_merge['start_date'].iloc[row] == datetime.date(
                    year=2018, month=12, day=31):
                asrun_merge['quarter'].iloc[row] = '2019 Q1'

        excel_start_date = datetime.date(1899, 12, 30)
        asrun_merge['start_date'] = asrun_merge['start_date'] - excel_start_date
        asrun_merge.start_date = asrun_merge.start_date.dt.days

        return asrun_merge

    def create_xlsx(self):
        merged = self.ispot_arl_matched_final.copy()
        merged = merged.rename(index=str, columns={'eq_units': 'Equivalized Units', 'brand_y': 'brand'})
        # subset merged data for output
        merged_sub = merged[[c for c in self.ispot_data_orig.columns] + ['Equivalized Units', 'campaign']].copy()
        self.unmatched_df['Equivalized Units'] = ''
        self.unmatched_df['campaign'] = ''
        merged_sub = pd.concat([merged_sub, self.unmatched_df], sort=False)
        merged_sub['ROS vs Optimized'] = merged_sub.campaign
        merged_sub['ROS vs Optimized'] = merged_sub['ROS vs Optimized'].replace({'Vantage': 'Optimized'})

        if self.client_name == 'T-Mobile':
            concurrent_ros = merged[merged.air_date_et > self.ros_concurrent_start_date]
        elif self.client_name == 'MetroPCS':
            concurrent_ros = merged[merged.air_date_et > self.ros_concurrent_start_date]
        else:
            concurrent_ros = merged
        concurrent_ros = concurrent_ros[concurrent_ros.campaign != 'Vantage']
        concurrent_ros['campaign'] = 'Concurrent ROS'
        merged = pd.concat([merged, concurrent_ros], axis=0)

        # create excel
        writer = pd.ExcelWriter(self.filename.split('.xlsx')[0] + '_ispot_analysis_report.xlsx',
                                engine='xlsxwriter')
        workbook = writer.book
        formatting = Formats(workbook)
        # pd.formats.format.header_style = None

        # create overall tab
        writer = create_overall_tab(writer, formatting, merged)
        merged = merged[merged.campaign != 'Concurrent ROS']
        writer = create_weekly_network_tab(writer, formatting, merged)
        writer = create_weekly_daypart_tab(writer, formatting, merged)
        writer = create_weekly_net_dp_tab(writer, formatting, merged)
        writer = create_program_tab(writer, formatting, merged)
        merged_sub.to_excel(writer, sheet_name='Raw Data', index=False)

        writer.save()


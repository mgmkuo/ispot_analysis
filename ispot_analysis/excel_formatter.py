class Formats:
    def __init__(self, workbook):
        self.workbook = workbook

    @property
    def percentage_format(self):
        return self.workbook.add_format({'num_format': '0.00%'})

    @property
    def float_format(self):
        return self.workbook.add_format({'num_format': '#,##0.0'})

    @property
    def date_format(self):
        return self.workbook.add_format({'align': 'center', 'valign': 'vcenter', 'num_format': 'mm/dd/yy'})

    @property
    def center_format(self):
        center_format = self.workbook.add_format()
        center_format.set_align('center')
        center_format.set_align('vcenter')
        center_format.set_text_wrap()
        return center_format

    @property
    def main_header_format(self):
        main_header_format = self.workbook.add_format()
        main_header_format.set_bold()
        main_header_format.set_font_color('white')
        main_header_format.set_pattern(1)
        main_header_format.set_bg_color('#404040')
        main_header_format.set_align('center')
        main_header_format.set_align('vcenter')
        main_header_format.set_text_wrap()
        main_header_format.set_border()
        return main_header_format

    @property
    def header_format(self):
        header_format = self.workbook.add_format()
        header_format.set_bold()
        header_format.set_font_color("white")
        header_format.set_pattern(1)
        header_format.set_bg_color('#707070')
        header_format.set_align('center')
        header_format.set_align('vcenter')
        header_format.set_text_wrap()
        header_format.set_border()
        return header_format

    @property
    def subheader_format(self):
        subheader_format = self.workbook.add_format()
        subheader_format.set_bold()
        subheader_format.set_pattern(1)
        subheader_format.set_bg_color('#A0A0A0')
        subheader_format.set_align('center')
        subheader_format.set_align('vcenter')
        subheader_format.set_border()
        subheader_format.set_text_wrap()
        return subheader_format

    @property
    def border_format(self):
        border_format = self.workbook.add_format()
        border_format.set_pattern(1)
        border_format.set_align('center')
        border_format.set_align('vcenter')
        border_format.set_text_wrap()
        border_format.set_border()
        return border_format

    @property
    def rowheader_format(self):
        rowheader_format = self.workbook.add_format()
        rowheader_format.set_bold()
        rowheader_format.set_pattern(1)
        rowheader_format.set_bg_color('#D0D0D0')
        rowheader_format.set_align('center')
        rowheader_format.set_align('vcenter')
        rowheader_format.set_text_wrap()
        rowheader_format.set_border()
        return rowheader_format

    @property
    def white_bold_format(self):
        white_bold_format = self.workbook.add_format()
        white_bold_format.set_bold()
        white_bold_format.set_bg_color("white")
        white_bold_format.set_border()
        white_bold_format.set_align('center')
        white_bold_format.set_align('vcenter')
        white_bold_format.set_text_wrap()
        return white_bold_format

    @property
    def white_format(self):
        white_format = self.workbook.add_format()
        white_format.set_bg_color("white")
        white_format.set_border()
        white_format.set_align('center')
        white_format.set_align('vcenter')
        white_format.set_text_wrap()
        return white_format

    @property
    def blue_bold_format(self):
        blue_bold_format = self.workbook.add_format()
        blue_bold_format.set_bold()
        blue_bold_format.set_bg_color("#BDD7EE")
        blue_bold_format.set_border()
        blue_bold_format.set_align('center')
        blue_bold_format.set_align('vcenter')
        blue_bold_format.set_text_wrap()
        return blue_bold_format

    @property
    def blue_format(self):
        blue_format = self.workbook.add_format()
        blue_format.set_bg_color("#DDEBF7")
        blue_format.set_border()
        blue_format.set_align('center')
        blue_format.set_align('vcenter')
        blue_format.set_text_wrap()
        return blue_format

    @property
    def green_bold_format(self):
        green_bold_format = self.workbook.add_format()
        green_bold_format.set_bold()
        green_bold_format.set_bg_color("#A9D08E")
        green_bold_format.set_border()
        green_bold_format.set_align('center')
        green_bold_format.set_align('vcenter')
        green_bold_format.set_text_wrap()
        return green_bold_format

    @property
    def green_format(self):
        green_format = self.workbook.add_format()
        green_format.set_bg_color("#E2EFDA")
        green_format.set_border()
        green_format.set_align('center')
        green_format.set_align('vcenter')
        green_format.set_text_wrap()
        return green_format
import pandas as pd

ORDERED_DAYPARTS = ["6:00AM - 8:59AM", "9:00AM - 3:59PM", "4:00PM - 6:59PM", "7:00PM - 10:59PM",
                    "11:00PM - 2:59AM", "3:00AM - 5:59AM"]


def create_weekly_net_dp_tab(writer, formats, merged_data):
    row_number = 1
    # weekly breakdowns
    # get ordered list of weeks
    weeks = list(['Total']) + sorted(merged_data.start_date.unique())
    week_col = chr(len(weeks) + 97 + 2).upper() if len(weeks) < 25 else \
        (chr(int((len(weeks) + 1 + 2) / 26 + 97 - 1)) + chr(int((len(weeks) + 1 + 1) % 26 + 97))).upper()

    # daypart weekly results
    # for each daypart
    weekly_data = merged_data[merged_data.campaign != 'Concurrent ROS']
    weekly_data = weekly_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units',
                               'campaign', 'network', 'vn_daypart', 'start_date']]
    pd.DataFrame().to_excel(writer, sheet_name='Weekly Network Daypart')
    tab = writer.sheets['Weekly Network Daypart']
    tab.hide_gridlines(2)
    tab.set_column('A:A', 16, formats.center_format)
    tab.set_column('B:B', 10, formats.center_format)
    tab.set_column('C:C', 20, formats.center_format)
    tab.set_column('D:' + week_col, 10, formats.center_format)

    network_list = sorted(weekly_data.network.unique())
    for net in network_list:
        print(net)
        weekly_net_data = weekly_data[weekly_data['network'] == net]
        weekly_net_results = weekly_net_data.groupby(['campaign', 'start_date', 'vn_daypart']).sum().reset_index()
        weekly_dp_spc_total_results = weekly_net_results.groupby(['start_date', 'vn_daypart']).sum().reset_index()
        weekly_dp_spc_total_results['campaign'] = 'Total'
        weekly_net_results = weekly_net_results.append(weekly_dp_spc_total_results, ignore_index=True)

        network_quarter_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'network', 'campaign', 'quarter','vn_daypart']]
        network_quarter_data = network_quarter_data[network_quarter_data['network'] == net]
        network_quarter_data = network_quarter_data.groupby(['campaign','quarter','vn_daypart']).sum().reset_index()
        network_quarter_data = network_quarter_data.rename(index=str, columns={'quarter':'start_date'})

        network_total_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units','campaign','network','vn_daypart']]
        network_total_data = network_total_data[network_total_data.network == net]
        network_total_data['start_date'] = 'Total'
        network_total_data = network_total_data.groupby(['start_date','campaign','vn_daypart']).sum().reset_index()
        network_quarter_data = network_quarter_data.append(network_total_data)

        network_total_row_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'quarter', 'network','vn_daypart']]
        network_total_row_data = network_total_row_data[network_total_row_data.network == net]
        network_total_row_data['campaign'] = 'Total'
        network_total_row_data = network_total_row_data.groupby(['campaign','quarter','vn_daypart']).sum().reset_index()
        network_total_row_data = network_total_row_data.rename(index=str, columns={'quarter':'start_date'})

        network_total_total_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'start_date', 'campaign', 'network','vn_daypart']]
        network_total_total_data = network_total_total_data[network_total_total_data.network == net]
        network_total_total_data['start_date'] = 'Total'
        network_total_total_data['campaign'] = 'Total'
        network_total_total_data = network_total_total_data.groupby(['campaign','start_date','vn_daypart']).sum().reset_index()

        weekly_net_results = weekly_net_results.append(network_total_total_data, ignore_index=True)
        weekly_net_results = weekly_net_results.append(network_total_row_data, ignore_index=True)
        weekly_net_results = weekly_net_results.append(network_quarter_data, ignore_index=True)

        print(weekly_net_results)


        weekly_net_results['Conversion Rate'] = weekly_net_results['matched_conversion_events'] / weekly_net_results['matched_tv_population_impressions']
        weekly_net_results["campaign"] = weekly_net_results["campaign"].astype('category',
                                                                               categories=["ROS", "Vantage", "Total"])
        weekly_net_results["vn_daypart"] = weekly_net_results["vn_daypart"].astype('category',
                                                                                   categories=ORDERED_DAYPARTS)
        weekly_dp_spc_pivot = weekly_net_results.pivot_table(index=['vn_daypart', "campaign", 'start_date'],
                                                             values=["Conversion Rate", 'Equivalized Units'])
        weekly_dp_spc_pivot = weekly_dp_spc_pivot.stack().unstack(-2).reset_index()
        weekly_dp_spc_pivot.rename(columns={'level_2': ' '}, inplace=True)
        weekly_dp_spc_pivot = weekly_dp_spc_pivot.reindex(columns=['vn_daypart', 'campaign', ' '] + weeks)
        weekly_dp_spc_pivot.to_excel(writer, sheet_name='Weekly Network Daypart', startrow=row_number+1, index=False)
        tab.merge_range('A' + str(row_number + 1) + ':' + week_col + str(row_number + 1), net, formats.header_format)
        tab.write('A' + str(row_number + 2), 'Daypart', formats.subheader_format)
        tab.merge_range('B' + str(row_number + 2) + ':C' + str(row_number + 2), ' ', formats.subheader_format)
        tab.conditional_format('D' + str(row_number + 2) + ':' + week_col + str(row_number + 2),
                               {'type': 'no_errors', 'format': formats.subheader_format})
        tab.conditional_format('D' + str(row_number + 2) + ':' + week_col + str(row_number + 2),
                               {'type': 'no_errors', 'format': formats.date_format})
        # merge cells for daypart col
        for d in weekly_dp_spc_pivot.vn_daypart.unique():
            ind = weekly_dp_spc_pivot.loc[weekly_dp_spc_pivot['vn_daypart'] == d].index
            tab.merge_range('A' + str(row_number + 3 + min(ind)) + ':A' + str(row_number + 3 + max(ind)), d,
                            formats.white_bold_format)

        tab.conditional_format('C' + str(row_number + 3) + ':C' + str(row_number + len(weekly_dp_spc_pivot) + 3),
                               {'type': 'no_errors', 'format': formats.white_bold_format})

        start_row = 0
        row_number += 2
        while start_row + 2 <= len(weekly_dp_spc_pivot):
            print(net)
            if weekly_dp_spc_pivot.loc[start_row]['campaign'] == weekly_dp_spc_pivot.loc[start_row + 1]['campaign']:
                campaign = weekly_dp_spc_pivot.loc[start_row]['campaign']
                print(campaign)
                print(start_row)
                tab.merge_range('B' + str(row_number + 1) + ':B' + str(row_number + 2), campaign,
                                formats.rowheader_format)
                tab.conditional_format('D' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                       {'type': 'no_errors', 'format': formats.percentage_format})
                tab.conditional_format('D' + str(row_number + 2) + ':' + week_col + str(row_number + 2),
                                       {'type': 'no_errors', 'format': formats.float_format})
                if campaign == 'Total':
                    tab.conditional_format('D' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                           {'type': 'no_errors', 'format': formats.blue_bold_format})
                    tab.conditional_format('D' + str(row_number + 2) + ':' + week_col + str(row_number + 2),
                                           {'type': 'no_errors', 'format': formats.green_bold_format})
                else:
                    tab.conditional_format('D' + str(row_number + 1) + ':' + week_col + str(row_number + 1),
                                           {'type': 'no_errors', 'format': formats.blue_format})
                    tab.conditional_format('D' + str(row_number + 2) + ':' + week_col + str(row_number + 2),
                                           {'type': 'no_errors', 'format': formats.green_format})
            start_row += 2
            row_number += 2
        row_number += 1

    return writer

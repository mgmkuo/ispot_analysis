import numpy as np
import string

ORDERED_DAYPARTS = ["6:00AM - 8:59AM", "9:00AM - 3:59PM", "4:00PM - 6:59PM", "7:00PM - 10:59PM",
                    "11:00PM - 2:59AM", "3:00AM - 5:59AM"]


def create_overall_tab(writer, formats, merged_data):
    row_number = 1
    # overall results

    subcategories = ["ROS", "Concurrent ROS", "Vantage"]
    subcategories.extend(np.sort(merged_data.quarter.unique()))
    subcategories.append('Total')

    mid_big_table_column = ':' + string.ascii_uppercase[len(subcategories)]
    end_big_table_column = ':' + string.ascii_uppercase[2 * len(subcategories)]

    overall_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'campaign']]
    overall_results = overall_data.groupby('campaign').sum().reset_index()
    total_results = merged_data[merged_data.campaign != 'Concurrent ROS']
    total_results = total_results[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units']].sum()
    total_results['campaign'] = 'Total'
    overall_results = overall_results.append(total_results, ignore_index=True)
    overall_results['Conversion Rate'] = overall_results['matched_conversion_events'] / overall_results['matched_tv_population_impressions']
    overall_results = overall_results[['campaign', 'Conversion Rate', 'Equivalized Units']].transpose()
    overall_results = overall_results.rename(columns=overall_results.iloc[0]).drop(overall_results.index[0])

    # write to excel
    overall_results.to_excel(writer, sheet_name='Overall', startrow=row_number)
    overall_tab = writer.sheets['Overall']
    overall_tab.hide_gridlines(2)
    overall_tab.set_column('A:A', 17, formats.center_format)
    overall_tab.set_column('B' + end_big_table_column, 15, formats.center_format)

    # format overall tab
    overall_tab.merge_range('A{row_num}:E{row_num}'.format(row_num=row_number), 'Overall Results', formats.main_header_format)
    overall_tab.conditional_format('A' + str(row_number + 1) + ':E' + str(row_number + 1),
                                   {'type': 'no_errors', 'format': formats.subheader_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':A' + str(row_number + len(overall_results) + 1),
                                   {'type': 'no_errors', 'format': formats.white_bold_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':E' + str(row_number + 1 + 1),
                                   {'type': 'no_errors', 'format': formats.percentage_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':D' + str(row_number + 1 + 1),
                                   {'type': '3_color_scale', 'min_color': '#C80000',
                                    'mid_color': '#ffffff', 'max_color': '#00C800'})
    overall_tab.conditional_format('A' + str(row_number + len(overall_results) + 1) + ':E' + str(row_number + len(overall_results) + 1),
                                   {'type': 'no_errors', 'format': formats.float_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':E' + str(row_number + len(overall_results) + 1),
                                   {'type': 'no_errors', 'format': formats.white_format})
    # add rows for titles and space
    row_number += len(overall_results) + 3

    quarter_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'campaign', 'quarter']]
    quarterly_results = quarter_data.groupby(['campaign', 'quarter']).sum().reset_index()
    quarter_totals = merged_data[merged_data.campaign != 'Concurrent ROS']
    quarter_totals = quarter_totals[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'quarter']]

    quarter_total_results = quarter_totals.groupby('quarter').sum().reset_index()
    quarter_total_results['campaign'] = 'Total'
    quarterly_results = quarterly_results.append(quarter_total_results, ignore_index=True)
    quarterly_results['Conversion Rate'] = quarterly_results['matched_conversion_events'] / quarterly_results['matched_tv_population_impressions']
    quarterly_results["campaign"] = quarterly_results["campaign"].astype('category', categories=["ROS", "Concurrent ROS", "Vantage", "Total"])
    quarter_pivot = quarterly_results.pivot_table(index='quarter', columns='campaign', values=['Conversion Rate', 'Equivalized Units'])

    quarter_pivot.to_excel(writer, sheet_name='Overall', startrow=row_number)
    overall_tab.merge_range('A{row_num}:I{row_num}'.format(row_num=row_number), 'Quarter Breakdown',
                            formats.main_header_format)
    overall_tab.conditional_format('A' + str(row_number + 1) + ':I' + str(row_number + 1),
                                   {'type': 'no_errors', 'format': formats.header_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':I' + str(row_number + 2),
                                   {'type': 'no_errors', 'format': formats.subheader_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':E' + str(row_number + len(quarter_pivot) + 3),
                                   {'type': '3_color_scale', 'min_color': '#C80000',
                                    'mid_color': '#ffffff', 'max_color': '#00C800', 'format': formats.border_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':A' + str(row_number + len(quarter_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_bold_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':E' + str(row_number + len(quarter_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':E' + str(row_number + len(quarter_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.percentage_format})
    overall_tab.conditional_format('F' + str(row_number + 1 + 1) + ':I' + str(row_number + len(quarter_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.float_format})
    overall_tab.conditional_format('F' + str(row_number + 1 + 1) + ':I' + str(row_number + len(quarter_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})
    overall_tab.write('A' + str(row_number + 2), ' ')
    overall_tab.set_row(row_number + 1, 30)
    overall_tab.set_row(row_number + 2, None, None, {'hidden': True})

    # add rows for titles and space
    row_number += len(quarter_pivot) + 5

    # network overall results
    network_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'campaign', 'network']]
    network_results = network_data.groupby(['campaign', 'network']).sum().reset_index()
    network_totals = merged_data[merged_data.campaign != 'Concurrent ROS']
    network_totals = network_totals[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'network']]
    network_total_results = network_totals.groupby('network').sum().reset_index()
    network_total_results['campaign'] = 'Total'
    network_results = network_results.append(network_total_results, ignore_index=True)

    network_quarter_results = merged_data[merged_data.campaign != 'Concurrent ROS']
    network_quarter_results = network_quarter_results[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'network','quarter']]
    network_quarter_results = network_quarter_results.groupby(['network','quarter']).sum().reset_index()
    network_quarter_results = network_quarter_results.rename(index=str, columns={'quarter': 'campaign'})
    network_results = network_results.append(network_quarter_results, ignore_index=True)

    network_results['Conversion Rate'] = network_results['matched_conversion_events'] / network_results['matched_tv_population_impressions']

    network_results["campaign"] = network_results["campaign"].astype('category', categories=subcategories)
    network_pivot = network_results.pivot_table(index='network', columns='campaign', values=['Conversion Rate', 'Equivalized Units'])

    network_pivot.to_excel(writer, sheet_name='Overall', startrow=row_number)
    overall_tab.merge_range('A{row_num}'.format(row_num=row_number) + end_big_table_column + '{row_num}'.format(row_num=row_number), 'Network Breakdown', formats.main_header_format)
    overall_tab.conditional_format('A' + str(row_number + 1) + end_big_table_column + str(row_number + 1),
                                   {'type': 'no_errors', 'format': formats.header_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + end_big_table_column + str(row_number + 2),
                                   {'type': 'no_errors', 'format': formats.subheader_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + mid_big_table_column + str(row_number + len(network_pivot) + 3),
                                   {'type': '3_color_scale', 'min_color': '#C80000',
                                   'mid_color': '#ffffff', 'max_color': '#00C800', 'format': formats.border_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':A' + str(row_number + len(network_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_bold_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + mid_big_table_column + str(row_number + len(network_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + mid_big_table_column + str(row_number + len(network_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.percentage_format})
    overall_tab.conditional_format('G' + str(row_number + 1 + 1) + end_big_table_column + str(row_number + len(network_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.float_format})
    overall_tab.conditional_format('G' + str(row_number + 1 + 1) + end_big_table_column + str(row_number + len(network_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})
    overall_tab.write('A' + str(row_number + 2), ' ')
    overall_tab.set_row(row_number + 1, 30)
    overall_tab.set_row(row_number + 2, None, None, {'hidden': True})
    row_number += len(network_pivot) + 5

    # daypart overall results
    daypart_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units',
                                'campaign', 'vn_daypart']]
    daypart_results = daypart_data.groupby(['campaign', 'vn_daypart']).sum().reset_index()
    daypart_totals = merged_data[merged_data.campaign != 'Concurrent ROS']
    daypart_totals = daypart_totals[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'vn_daypart']]
    daypart_total_results = daypart_totals.groupby('vn_daypart').sum().reset_index()
    daypart_total_results['campaign'] = 'Total'
    daypart_results = daypart_results.append(daypart_total_results, ignore_index=True)

    daypart_quarter_results = merged_data[merged_data.campaign != 'Concurrent ROS']
    daypart_quarter_results = daypart_quarter_results[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'vn_daypart','quarter']]
    daypart_quarter_results = daypart_quarter_results.groupby(['vn_daypart','quarter']).sum().reset_index()
    daypart_quarter_results = daypart_quarter_results.rename(index=str, columns={'quarter': 'campaign'})
    daypart_results = daypart_results.append(daypart_quarter_results, ignore_index=True)


    daypart_results['Conversion Rate'] = daypart_results['matched_conversion_events'] / daypart_results['matched_tv_population_impressions']
    daypart_results["campaign"] = daypart_results["campaign"].astype('category', categories=subcategories)
    daypart_results["vn_daypart"] = daypart_results["vn_daypart"].astype('category', categories=ORDERED_DAYPARTS)
    daypart_pivot = daypart_results.pivot_table(index='vn_daypart', columns='campaign', values=['Conversion Rate', 'Equivalized Units'])

    daypart_pivot.to_excel(writer, sheet_name='Overall', startrow=row_number)
    overall_tab.merge_range('A{row_num}'.format(row_num=row_number) + end_big_table_column + '{row_num}'.format(row_num=row_number), 'Daypart Breakdown', formats.main_header_format)
    overall_tab.conditional_format('A' + str(row_number + 1) + end_big_table_column + str(row_number + 1),
                                   {'type': 'no_errors', 'format': formats.header_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + end_big_table_column + str(row_number + 2),
                                   {'type': 'no_errors', 'format': formats.subheader_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + mid_big_table_column + str(row_number + len(daypart_pivot) + 3),
                                   {'type': '3_color_scale', 'min_color': '#C80000',
                                    'mid_color': '#ffffff', 'max_color': '#00C800'})
    overall_tab.conditional_format('A' + str(row_number + 2) + mid_big_table_column + str(row_number + len(daypart_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + mid_big_table_column + str(row_number + len(daypart_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.percentage_format})
    overall_tab.conditional_format('G' + str(row_number + 1 + 1) + end_big_table_column + str(row_number + len(daypart_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.float_format})
    overall_tab.conditional_format('G' + str(row_number + 1 + 1) + end_big_table_column + str(row_number + len(daypart_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})
    overall_tab.write('A' + str(row_number + 2), ' ')
    overall_tab.set_row(row_number + 1, 30)
    overall_tab.set_row(row_number + 2, None, None, {'hidden': True})
    row_number += len(daypart_pivot) + 5

    # network/daypart overall breakdown

    # ros
    ros = merged_data[merged_data['campaign'] == 'ROS']
    ros_data = ros[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'network', 'vn_daypart']]
    ros_results = ros_data.groupby(['network', 'vn_daypart']).sum().reset_index()
    ros_results['Conversion Rate'] = ros_results['matched_conversion_events'] / ros_results['matched_tv_population_impressions']
    ros_results["vn_daypart"] = ros_results["vn_daypart"].astype('category', categories=ORDERED_DAYPARTS)
    ros_pivot = ros_results.pivot_table(index='network', columns='vn_daypart', values=['Conversion Rate', 'Equivalized Units'])

    end_col = chr(ord('A') + ros_pivot.shape[1])
    end_col_conv = chr(ord('A') + ros_pivot['Conversion Rate'].shape[1])

    ros_pivot.to_excel(writer, sheet_name='Overall', startrow=row_number)
    overall_tab.merge_range('A{row_num}:{col_letter}{row_num}'.format(row_num=row_number, col_letter=end_col), 'ROS Network/Daypart Breakdown', formats.main_header_format)
    overall_tab.conditional_format('A' + str(row_number + 1) + ':' + end_col + str(row_number + 1),
                                   {'type': 'no_errors', 'format': formats.header_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':' + end_col + str(row_number + 2),
                                   {'type': 'no_errors', 'format': formats.subheader_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':A' + str(row_number + len(ros_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_bold_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col_conv + str(row_number + len(ros_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.percentage_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col_conv + str(row_number + len(ros_pivot) + 3),
                                   {'type': '3_color_scale', 'min_color': '#C80000',
                                    'mid_color': '#ffffff', 'max_color': '#00C800'})
    overall_tab.conditional_format('E' + str(row_number + 1 + 1) + ':' + end_col + str(row_number + len(ros_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.float_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col + str(row_number + len(ros_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})
    overall_tab.write('A' + str(row_number + 2), ' ')
    overall_tab.set_row(row_number + 1, 30)
    overall_tab.set_row(row_number + 2, None, None, {'hidden': True})
    row_number += len(ros_pivot) + 5

    # concurrent ros
    ros = merged_data[merged_data['campaign'] == 'Concurrent ROS']
    ros_data = ros[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'network',
                    'vn_daypart']]
    ros_results = ros_data.groupby(['network', 'vn_daypart']).sum().reset_index()
    ros_results['Conversion Rate'] = ros_results['matched_conversion_events'] / ros_results[
        'matched_tv_population_impressions']
    ros_results["vn_daypart"] = ros_results["vn_daypart"].astype('category', categories=ORDERED_DAYPARTS)
    ros_pivot = ros_results.pivot_table(index='network', columns='vn_daypart',
                                        values=['Conversion Rate', 'Equivalized Units'])

    end_col = chr(ord('A') + ros_pivot.shape[1])
    end_col_conv = chr(ord('A') + ros_pivot['Conversion Rate'].shape[1])

    ros_pivot.to_excel(writer, sheet_name='Overall', startrow=row_number)
    overall_tab.merge_range('A{row_num}:{col_letter}{row_num}'.format(row_num=row_number, col_letter=end_col), 'Concurrent ROS Network/Daypart Breakdown',
                            formats.main_header_format)
    overall_tab.conditional_format('A' + str(row_number + 1) + ':' + end_col + str(row_number + 1),
                                   {'type': 'no_errors', 'format': formats.header_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':' + end_col + str(row_number + 2),
                                   {'type': 'no_errors', 'format': formats.subheader_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':A' + str(row_number + len(ros_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_bold_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col_conv + str(row_number + len(ros_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.percentage_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col_conv + str(row_number + len(ros_pivot) + 3),
                                   {'type': '3_color_scale', 'min_color': '#C80000',
                                    'mid_color': '#ffffff', 'max_color': '#00C800'})
    overall_tab.conditional_format('G' + str(row_number + 1 + 1) + ':' + end_col + str(row_number + len(ros_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.float_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col + str(row_number + len(ros_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})
    overall_tab.write('A' + str(row_number + 2), ' ')
    overall_tab.set_row(row_number + 1, 30)
    overall_tab.set_row(row_number + 2, None, None, {'hidden': True})
    row_number += len(ros_pivot) + 5

    # vantage
    vantage = merged_data[merged_data['campaign'] == 'Vantage']
    vantage_data = vantage[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'network', 'vn_daypart']]
    vantage_results = vantage_data.groupby(['network', 'vn_daypart']).sum().reset_index()
    vantage_results['Conversion Rate'] = vantage_results['matched_conversion_events'] / vantage_results['matched_tv_population_impressions']
    vantage_results["vn_daypart"] = vantage_results["vn_daypart"].astype('category', categories=ORDERED_DAYPARTS)
    vantage_pivot = vantage_results.pivot_table(index='network', columns='vn_daypart', values=['Conversion Rate', 'Equivalized Units'])

    end_col = chr(ord('A') + vantage_pivot.shape[1])
    end_col_conv = chr(ord('A') + vantage_pivot['Conversion Rate'].shape[1])

    vantage_pivot.to_excel(writer, sheet_name='Overall', startrow=row_number)
    overall_tab.merge_range('A{row_num}:{col_letter}{row_num}'.format(row_num=row_number, col_letter=end_col), 'Vantage Network/Daypart Breakdown', formats.main_header_format)
    overall_tab.conditional_format('A' + str(row_number + 1) + ':' + end_col + str(row_number + 1),
                                   {'type': 'no_errors', 'format': formats.header_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':' + end_col + str(row_number + 2),
                                   {'type': 'no_errors', 'format': formats.subheader_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col_conv + str(row_number + len(vantage_pivot) + 3),
                                   {'type': '3_color_scale', 'min_color': '#C80000',
                                    'mid_color': '#ffffff', 'max_color': '#00C800'})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':A' + str(row_number + len(vantage_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_bold_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col_conv + str(row_number + len(vantage_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.percentage_format})
    overall_tab.conditional_format('E' + str(row_number + 1 + 1) + ':' + end_col + str(row_number + len(vantage_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.float_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col + str(row_number + len(vantage_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})

    overall_tab.write('A' + str(row_number + 2), ' ')
    overall_tab.set_row(row_number + 1, 30)
    overall_tab.set_row(row_number + 2, None, None, {'hidden': True})
    row_number += len(vantage_pivot) + 5

    # total
    netdp = merged_data[merged_data.campaign != "Concurrent ROS"]
    netdp = netdp[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units', 'network', 'vn_daypart']]
    netdp_results = netdp.groupby(['network', 'vn_daypart']).sum().reset_index()
    netdp_results['Conversion Rate'] = netdp_results['matched_conversion_events'] / netdp_results['matched_tv_population_impressions']
    netdp_results["vn_daypart"] = netdp_results["vn_daypart"].astype('category', categories=ORDERED_DAYPARTS)
    netdp_pivot = netdp_results.pivot_table(index='network', columns='vn_daypart', values=['Conversion Rate', 'Equivalized Units'])

    end_col = chr(ord('A') + netdp_pivot.shape[1])
    end_col_conv = chr(ord('A') + vantage_pivot['Conversion Rate'].shape[1])

    netdp_pivot.to_excel(writer, sheet_name='Overall', startrow=row_number)
    overall_tab.merge_range('A{row_num}:{col_letter}{row_num}'.format(row_num=row_number, col_letter=end_col), 'Total Network/Daypart Breakdown', formats.main_header_format)
    overall_tab.conditional_format('A' + str(row_number + 1) + ':' + end_col + str(row_number + 1),
                                   {'type': 'no_errors', 'format': formats.header_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':' + end_col + str(row_number + 2),
                                   {'type': 'no_errors', 'format': formats.subheader_format})
    overall_tab.conditional_format('A' + str(row_number + 2) + ':A' + str(row_number + len(netdp_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_bold_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col_conv + str(row_number + len(netdp_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.percentage_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col_conv + str(row_number + len(netdp_pivot) + 3),
                                   {'type': '3_color_scale', 'min_color': '#C80000',
                                    'mid_color': '#ffffff', 'max_color': '#00C800'})
    overall_tab.conditional_format('E' + str(row_number + 1 + 1) + ':' + end_col + str(row_number + len(netdp_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.float_format})
    overall_tab.conditional_format('A' + str(row_number + 1 + 1) + ':' + end_col + str(row_number + len(netdp_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.white_format})
    overall_tab.write('A' + str(row_number + 2), ' ')
    overall_tab.set_row(row_number + 1, 30)
    overall_tab.set_row(row_number + 2, None, None, {'hidden': True})
    row_number += len(netdp) + 5

    return writer

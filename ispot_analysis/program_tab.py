
def create_program_tab(writer, formats, merged_data):

    program_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events', 'Equivalized Units',
                                'campaign', 'program_posted_title']]
    program_results = program_data.groupby(['campaign', 'program_posted_title']).sum().reset_index()
    total_program_data = merged_data[['matched_tv_population_impressions', 'matched_conversion_events',
                                      'Equivalized Units', 'program_posted_title']]
    total_program_results = total_program_data.groupby(['program_posted_title']).sum().reset_index()
    total_program_results['campaign'] = 'Total'
    program_results = program_results.append(total_program_results, ignore_index=True)
    program_results['Conversion Rate'] = program_results['matched_conversion_events'] / program_results['matched_tv_population_impressions']
    program_results["campaign"] = program_results["campaign"].astype('category', categories=["ROS", "Vantage", "Total"])
    # pivot the data
    program_pivot = program_results.pivot_table(index='program_posted_title', columns='campaign',
                                                values=['Conversion Rate', 'Equivalized Units'])

    program_pivot.to_excel(writer, sheet_name='Program Level')

    # format
    program_tab = writer.sheets['Program Level']
    program_tab.hide_gridlines(2)
    program_tab.set_column('A:A', 70)
    program_tab.set_column('B:G', 10, formats.center_format)
    program_tab.set_row(2, None, None, {'hidden': True})

    program_tab.conditional_format('A1:G1', {'type': 'no_errors', 'format': formats.header_format})
    program_tab.write('A2', 'Program')
    program_tab.conditional_format('A2:G2', {'type': 'no_errors', 'format': formats.subheader_format})
    program_tab.conditional_format('B3:D' + str(len(program_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.percentage_format})
    program_tab.conditional_format('E3:G' + str(len(program_pivot) + 3),
                                   {'type': 'no_errors', 'format': formats.float_format})
    for row in range(len(program_pivot)):
        program_tab.conditional_format('A' + str(row + 4) + ':G' + str(row + 4),
                                       {'type': 'formula', 'criteria': '=$G' + str(row + 4) + '>=10',
                                        'format': formats.green_bold_format})
        program_tab.conditional_format('A' + str(row + 4) + ':G' + str(row + 4),
                                       {'type': 'formula', 'criteria': '=$G' + str(row + 4) + '<10',
                                        'format': formats.white_format})

    return writer

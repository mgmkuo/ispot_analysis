"""
Enter parameters to generate iSpot report

Report is saved to the same directory as the input file.


To execute:
for iSpot identify:
python run_identify.py if have environment with the dependencies needed

for iSpot analysis:
python run_analysis.py

Or to use poetry, can do:
poetry install (Creates the virtualenv environment. Only have to do once.)
poetry run python run_identify.py


"""

filename = 'filename.xlsx'
client_name = 'ClientName'
ros_concurrent_start_date = '2018-12-31'  # (May change with client; will be ignored for iSpot identify)

# redshift creds
user_name = 'user_name'
password = 'password'
